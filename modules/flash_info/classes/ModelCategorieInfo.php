<?php

/**
 * 
 */
class ModelCategorieInfo extends ObjectModel
{
	
	public $active = true;
	public $category;

	public $description;

	/**
     * @see ObjectModel::$definition
     */

	public static $definition =array(
		'table' => 'category_info',
		'primary' => 'id_category_info',
		'multilang' => true,
		'fields' => array(
			'category' => array('type' => self::TYPE_HTML, 'required' => true),
			'active' => array('type' =>self::TYPE_BOOL),
             /* Lang fields */
            'description' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
		),
	);
	public static function getcategoryinfo( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('category_info')
    		->where ('active='.(int)$active);
    	return Db::getInstance() ->executeS($q); 
	}
	public static function getCategoryInfoById($id)
    {
    	$q= new DbQuery();
    	$q->select('category')
    		->from('category_info')
			->where ('id_category_info='.$id);
		$instance= Db::getInstance() ->executeS($q);
		$temp = array();
		foreach ($instance as $value) {
				$temp = $value;
		}
		return $temp['category'] ; 
    }
}