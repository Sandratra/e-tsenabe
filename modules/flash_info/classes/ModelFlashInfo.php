<?php 

/**
 * 
 */
class ModelFlashInfo extends ObjectModel
{
	
	public $active = true;
	public $id_category_info;
	public $title;
	public $description;
	public $link;
	public $date_info;
	public $id_category_monde;
	/**
     * @see ObjectModel::$definition
     */
	public static $definition =array(
		'table' => 'flash_info',
		'primary' => 'id_flash_info',
		'multilang' => true,
		'fields' => array(
			'id_category_info' => array('type' => self::TYPE_HTML, 'required' => true),
			'id_category_monde' => array('type' => self::TYPE_HTML, 'required' => true),
			'title' => array('type' => self::TYPE_HTML, 'required' => true),
			'link' => array('type' => self::TYPE_HTML, 'required' => true),
			'date_info' => array('type' => self::TYPE_HTML, 'required' => true),
            'active' => array('type' =>self::TYPE_BOOL),


             /* Lang fields */
            'description' => array('type' => self::TYPE_HTML, 'lang' => true,'required' => true),
		),
	);

	public static $img_dir = _PS_IMG_DIR_.'flashinfo';

    public static function getImgPath($front=false){
        return  $front ? __PS_BASE_URI__.'img/flashinfo/' :__PS_BASE_URI__.'img/flashinfo/' ;
    }
     public static function getflashinfo($limite=5, $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('flash_info')
    		->Limit($limite)
    		->where ('active='.(int)$active);
            

    	return Db::getInstance() ->executeS($q); 
    }
}
