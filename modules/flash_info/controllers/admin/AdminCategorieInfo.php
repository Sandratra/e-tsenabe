<?php


/**
 * 
 */

if(!class_exists( 'ModelCategorieInfo'));
   require_once _PS_MODULE_DIR_.'flash_info/classes/ModelCategorieInfo.php';

class AdminCategorieInfoController extends ModuleAdminController
{
	
	public $bootstrap = true;
	public function __construct()
	{
		$this->table = 'category_info';
		$this->list_id = 'category_info';
		$this->className = 'ModelCategorieInfo';
		$this->lang = true;
		 $this->deleted = false;
		 $this->identifier = 'id_category_info';

		  parent::__construct();
		    $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
            ),
        );

		     /* Liste d'affichage*/
		$this->fields_list = array(
            'id_category_info' => array(
                'title' => $this->trans('ID',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'category' => array(
                'title' => $this->trans('Categorie d information',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
          /*  'create_date' => array(
                'title' => $this->trans('Date de Creation',array(), 'Admin.Global'),
                'width' => 'auto',
            ),*/
            'active' => array(
            	'title' => $this->trans('Enabled',array(), 'Admin.Global'),
            	'active' => 'status',
            	'type' => 'bool',
            	'align' => 'center',
            	'class' => 'fixed-width-xs',
            	'orderby' => false
            )  
        );
         /*Action modification et suppresion*/
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		/*parent:: construct();*/
	}
	//fin public construct

	//Formulaire d'ajout et de modification
	public function renderForm()
	{

        /*echo date("d/m/y");*/
        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('Ajout catégorie info', array(), 'Admin.Catalog.Feature'),
                'icon' => 'icon-certificate',
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->trans('Categorie d information', array(), 'Admin.Global'),
                    'name' => 'category',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
               
	            array(
	                    'type' => 'textarea',
	                    'label' => $this->trans('Description', array(), 'Admin.Global'),
	                    'name' => 'description',
	                    'lang' => true,
	                    'cols' => 60,
	                    'rows' => 10,
	                    'col' => 6,
	                    'autoload_rte' => 'rte', //Enable TinyMCE editor for description
	                    'hint' => $this->l('Invalid characters:', array(), 'Admin.Notifications.Info') . ' &lt;&gt;;=#{}',
	                ),             
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Enable', array(), 'Admin.Actions'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled', array(), 'Admin.Global'),
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled', array(), 'Admin.Global'),
                        ),
                    ),
                ),
            ),
        );
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save', array(), 'Admin.Actions'),
        );
         return parent::renderForm();
	}

	protected function l($string,$class = null,$addslashes = false,$htmlentities = true)
    {
    	if(_PS_VERSION_>='1.7'){
    		return Context::getContext()->getTranslator()->trans($string);
    	}else{
    		return parent::trans($string,$class,$addslashes,$htmlentities);
    	}

    }
}