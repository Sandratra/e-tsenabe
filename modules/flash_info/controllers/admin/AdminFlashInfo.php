<?php

/**
 * 
 */
if(!class_exists( 'ModelFlashInfo'));
   require_once _PS_MODULE_DIR_.'flash_info/classes/ModelFlashInfo.php';
if(!class_exists( 'ModelCategorieInfo'));
   require_once _PS_MODULE_DIR_.'flash_info/classes/ModelCategorieInfo.php';
class AdminFlashInfoController extends ModuleAdminController
{
	public $bootstrap = true;
	public function __construct()
	{
		$this->table = 'flash_info';
		$this->list_id = 'flash_info';
		$this->className = 'ModelFlashInfo';
		$this->lang = true;
		$this->deleted = false;
		$this->identifier = 'id_flash_info';

		parent::__construct();
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
            ),
        );
		$this->fieldImageSettings = array(
            'name'=>'image',
            'dir' => 'flashinfo'
        );

		     /* Liste d'affichage*/
		$this->fields_list = array(
            'id_flash_info' => array(
                'title' => $this->trans('ID',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'id_category_monde' => array(
                'title' => $this->trans('Id categorie monde',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'id_category_info' => array(
                'title' => $this->trans('Id type categorie',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'title' => array(
                'title' => $this->trans('Titre',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'link' => array(
                'title' => $this->trans('Lien',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            
            'date_info' => array(
                'title' => $this->trans('Date',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'active' => array(
            	'title' => $this->trans('Enabled',array(), 'Admin.Global'),
            	'active' => 'status',
            	'type' => 'bool',
            	'align' => 'center',
            	'class' => 'fixed-width-xs',
            	'orderby' => false
            )  
        );
         /*Action modification et suppresion*/
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		/*parent:: construct();*/
	}
	//fin public construct

	//Formulaire d'ajout et de modification
	public function renderForm()
	{
		
		if (!($flashinfo = $this->loadObject(true))) {
            return;
        }
        $image = ModelFlashInfo::$img_dir.'/'.$flashinfo->id . '.jpg';
        /*var_dump($image);*/
        $image_url = ImageManager::thumbnail(
            $image,
            $this->table . '_' . (int) $flashinfo->id . '.' . $this->imageType,
            350,
            $this->imageType,
            true,
            true
        );
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;


        //option select
        $categorieinfo = ModelCategorieInfo::getcategoryinfo(true);
        $listCateg = array();
        if (count($categorieinfo) > 0) {
            for ($i = 0 ; $i < count($categorieinfo) ; $i++) {
                $listCategObject['key'] = $categorieinfo[$i]['id_category_info'];
                $listCategObject['name'] = $categorieinfo[$i]['category'];
                array_push($listCateg, $listCategObject);
            }
        }

        //fin option select
        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('Ajout flash info', array(), 'Admin.Catalog.Feature'),
                'icon' => 'icon-certificate',
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->trans('Categorie monde', array(), 'Admin.Global'),
                    'name' => 'id_category_monde',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
			            'query' => array(
			                array('key' => '1', 'name' => 'Nationale'),
			                array('key' => '2', 'name' => 'Internationale'),
			            ),
			            'id' => 'key',
			            'name' => 'name'
        			)
                ),
            	array(
                    'type' => 'select',
                    'label' => $this->trans('Categorie Info', array(), 'Admin.Global'),
                    'name' => 'id_category_info',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
			            'query' => $listCateg,
			            'id' => 'key',
			            'name' => 'name'
        			)
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Titre', array(), 'Admin.Global'),
                    'name' => 'title',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Lien', array(), 'Admin.Global'),
                    'name' => 'link',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                array(
                    'type' => 'date',
                    'label' => $this->trans('Date d ajout', array(), 'Admin.Global'),
                    'name' => 'date_info',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
              
	            array(
	                    'type' => 'textarea',
	                    'label' => $this->trans('Description', array(), 'Admin.Global'),
	                    'name' => 'description',
	                    'lang' => true,
	                    'cols' => 60,
	                    'rows' => 10,
	                    'col' => 6,
	                    'autoload_rte' => 'rte', //Enable TinyMCE editor for description
	                    'hint' => $this->trans('Invalid characters:', array(), 'Admin.Catalog.Help') . ' &lt;&gt;;=#{}',
	                ),
	               
	            array(
	                    'type' => 'file',
	                    'label' => $this->trans('Image', array(), 'Admin.Global'),
	                    'name' => 'image',
	                    'image' => $image_url ? $image_url : false,
	                    'size' => $image_size,
	                    'display_image' => true,
	                    'col' => 6,
	                    'hint' => $this->trans('Upload a brand logo from your computer.', array(), 'Admin.Catalog.Help'),
	                ),
              
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Enable', array(), 'Admin.Actions'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled', array(), 'Admin.Global'),
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled', array(), 'Admin.Global'),
                        ),
                    ),
                ),
            ),
        );
		if (!($flashinfo = $this->loadObject(true))) {
		            return;
		        }
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save', array(), 'Admin.Actions'),
        );
         return parent::renderForm();
	}
	protected function l($string,$class = null,$addslashes = false,$htmlentities = true)
    {
    	if(_PS_VERSION_>='1.7'){
    		return Context::getContext()->getTranslator()->trans($string);
    	}else{
    		return parent::trans($string,$class,$addslashes,$htmlentities);
    	}

    }
}