{* <h2 class="h2 products-section-title text-uppercase" style="text-align:center;">Flash Info</h2>         *}
 
  <div class="card card_colonne" id="homeslider-container">
      <ul class="rslides">
      {foreach from=$flashinfo item=flash}
        {if $flash.id_category_monde == 1} 
          <li class="cardinfo"> 
            <h3 class="titre">Actualités</h3> 
            <img src="{$flash.flashinfo_path}{$flash.id_flash_info}.jpg" alt="Avatar" class="imginfo">
            <div class="col-md-10 descri">
              <p class="textedate"><span class="material-icons">event</span>{$flash.date}</p>
              <h5 class="textecatego">Actualité Nationale</h5>
              <p class="textecat">Categorie : {$flash.nom_category}</p>
              <p class="textetitre">{$flash.title}</p>
            </div>
            <div class="col-md-2 voir">
              <a href="{$flash.link}" class="textevoir"><span class="material-icons"  style="font-size:35px !important;">visibility</span></a>
            </div>
          </li> 
        {elseif $flash.id_category_monde == 2}      
          <li class="cardinfo">  
            <h3 class="titre">Actualités</h3> 
            <img src="{$flash.flashinfo_path}{$flash.id_flash_info}.jpg" alt="Avatar" class="imginfo">
            <div class="col-md-10 descri">
              <p class="textedate"><span class="material-icons">event</span>{$flash.date}</p>
              <h5 class="textecatego">Actualité Nationale</h5>
              <p class="textecat">Categorie : {$flash.nom_category}</p>
              <p class="textetitre">{$flash.title}</p>
            </div>
            <div class="col-md-2 voir">
              <a href="{$flash.link}" class="textevoir"><span class="material-icons view" >visibility</span></a>
            </div>
          </li> 
        {/if}  
      {/foreach} 
      </ul>
  </div>
</div> 
{* {literal}
  <script>
    $(document).ready(function(){
      // Activate Carousel
      $("#myCarousel").carousel();
        
      // Enable Carousel Indicators
      $(".item1").click(function(){
        $("#myCarousel").carousel(0);
      });
      $(".item2").click(function(){
        $("#myCarousel").carousel(1);
      });
      $(".item3").click(function(){
        $("#myCarousel").carousel(2);
      });
      $(".item4").click(function(){
        $("#myCarousel").carousel(3);
      });
    });
  </script>
{/literal} *}
