<?php

/**
 * 
 */
class ModelPharmacieGarde extends ObjectModel
{
	
	public $active=true;
	public $id_poi;
	public $date_garde;
	/**
     * @see ObjectModel::$definition
     */
	public static $definition =array(
		'table' => 'pharmacie_garde',
		'primary' => 'id',
		'multilang' => true,
		'fields' => array(
			'id_poi' => array('type' => self::TYPE_HTML, 'required' => true),
			'date_garde' =>array('type' => self::TYPE_HTML, 'required' => true),
			'active' => array('type' =>self::TYPE_BOOL),
             
		),
	);
	  public static function getpharmaciegarde( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('pharmacie_garde')
    		->where ('active='.(int)$active);
    	return Db::getInstance() ->executeS($q); 
    }
}