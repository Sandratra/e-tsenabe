<?php

/**
 * 
 */
class ModelPoiListe extends ObjectModel
{
	public $id_poi_type;
	public $active=true;
	public $poi_name;
	public $poi_adresse;
	/**
     * @see ObjectModel::$definition
     */
	public static $definition =array(
		'table' => 'poi_liste',
		'primary' => 'id_poi',
		'multilang' => true,
		'fields' => array(
			'id_poi_type' => array('type' => self::TYPE_HTML, 'required' => true),
			'poi_name' => array('type' => self::TYPE_HTML, 'required' => true),
			'poi_adresse' => array('type' => self::TYPE_HTML, 'required' => true),
			'active' => array('type' =>self::TYPE_BOOL),       
		),
	);
	  public static function getpoiliste( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('poi_liste')
    		->where ('active='.(int)$active);
    	return Db::getInstance() ->executeS($q); 
    }
	
}