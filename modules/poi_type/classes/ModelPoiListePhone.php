<?php

/**
 * 
 */
class ModelPoiListePhone extends ObjectModel
{

	public $id_poi;
	public $id_poi_type_phone;
	public $active=true;
	public $poi_telephone;
	/**
     * @see ObjectModel::$definition
     */
	public static $definition =array(
		'table' => 'poi_phone_liste',
		'primary' => 'id_phone',
		'multilang' => true,
		'fields' => array(
			'id_poi' => array('type' => self::TYPE_HTML, 'required' => true),
			'id_poi_type_phone' => array('type' => self::TYPE_HTML, 'required' => true),
			'poi_telephone' => array('type' => self::TYPE_HTML, 'required' => true),
			'active' => array('type' =>self::TYPE_BOOL),       
		),
	);
	  public static function getpoilistephone( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('poi_phone_liste')
    		->where ('active='.(int)$active);
    	return Db::getInstance() ->executeS($q); 
    }
	
	
}