<?php

/**
 * 
 */
class ModelPoiType extends ObjectModel
{
	public $active=true;
	public $poi_type;
	/**
     * @see ObjectModel::$definition
     */
	public static $definition =array(
		'table' => 'poi_type',
		'primary' => 'id_poi_type',
		'multilang' => true,
		'fields' => array(
			'poi_type' => array('type' => self::TYPE_HTML, 'required' => true),
			
			'active' => array('type' =>self::TYPE_BOOL),
             
		),
	);
	  public static function getpoitype( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('poi_type')
    		->where ('active='.(int)$active);
    	return Db::getInstance() ->executeS($q); 
    }
	
}