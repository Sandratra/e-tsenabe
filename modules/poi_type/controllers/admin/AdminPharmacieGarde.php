<?php

/**
 * 
 */
if(!class_exists( 'ModelPharmacieGarde'));
   require_once _PS_MODULE_DIR_.'poi_type/classes/ModelPharmacieGarde.php';
if(!class_exists( 'ModelPoiListe'));
   require_once _PS_MODULE_DIR_.'poi_type/classes/ModelPoiListe.php';
class AdminPharmacieGardeController extends ModuleAdminController
{
	public $bootstrap = true;

	public function __construct()
	{
		$this->table = 'pharmacie_garde';
		$this->list_id = 'pharmacie_garde';
		$this->className = 'ModelPharmacieGarde';
		$this->lang = true;
		 $this->deleted = false;
		 $this->identifier = 'id';

		  parent::__construct();
		    $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
            ),
        );

		     /* Liste d'affichage*/
		$this->fields_list = array(
            'id' => array(
                'title' => $this->trans('ID',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'id_poi' => array(
                'title' => $this->trans('Point d\'interêt',array(), 'Admin.Global'),
                'width' => 'auto',

            ),
            'date_garde' => array(
                'title' => $this->trans('Date de Garde',array(), 'Admin.Global'),
                'width' => 'auto',

            ),
            'active' => array(
            	'title' => $this->trans('Enabled',array(), 'Admin.Global'),
            	'active' => 'status',
            	'type' => 'bool',
            	'align' => 'center',
            	'class' => 'fixed-width-xs',
            	'orderby' => false
            )  
        );
         /*Action modification et suppresion*/
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		/*parent:: construct();*/
	}
	//Formulaire d'ajout et de modification
	public function renderForm()
	{

         //option select    
        $poiliste = ModelPoiListe::getpoiliste(true);
        $listepoi = array();
        if(count($poiliste)){
            for ($i=0; $i < count($poiliste) ; $i++) { 
            
                $listepoiobject['key'] = $poiliste[$i]['id_poi'];
                $listepoiobject['name'] = $poiliste[$i]['poi_name'];
                array_push($listepoi , $listepoiobject);
                

            }
        }
        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('Ajout pharmacie de garde', array(), 'Admin.Catalog.Feature'),
                'icon' => 'icon-certificate',
            ),
            'input' => array(
            	  array(
                    'type' => 'select',
                    'label' => $this->trans('Point d\'interêt ', array(), 'Admin.Global'),
                    'name' => 'id_poi',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
			            'query' => $listepoi,
			            'id' => 'key',
			            'name' => 'name'
        			)
                ),
              
                array(
                    'type' => 'date',
                    'label' => $this->trans('Date Garde', array(), 'Admin.Global'),
                    'name' => 'date_garde',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
	             
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Enable', array(), 'Admin.Actions'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled', array(), 'Admin.Global'),
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled', array(), 'Admin.Global'),
                        ),
                    ),
                ),
            ),
        );
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save', array(), 'Admin.Actions'),
        );
         return parent::renderForm();
	}
	protected function l($string,$class = null,$addslashes = false,$htmlentities = true)
    {
    	if(_PS_VERSION_>='1.7'){
    		return Context::getContext()->getTranslator()->trans($string);
    	}else{
    		return parent::trans($string,$class,$addslashes,$htmlentities);
    	}

    }
}