<?php

/**
 * 
 */

if(!class_exists( 'ModelPoiListe'));
   require_once _PS_MODULE_DIR_.'poi_type/classes/ModelPoiListe.php';
if(!class_exists( 'ModelPoiType'));
   require_once _PS_MODULE_DIR_.'poi_type/classes/ModelPoiType.php';
class AdminPoiListeController extends ModuleAdminController
{
	
	public $bootstrap = true;
	public function __construct()
	{
		$this->table = 'poi_liste';
		$this->list_id = 'poi_liste';
		$this->className = 'ModelPoiListe';
		$this->lang = true;
		 $this->deleted = false;
		 $this->identifier = 'id_poi';

		  parent::__construct();
		    $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
            ),
        );

		     /* Liste d'affichage*/
		$this->fields_list = array(
            'id_poi' => array(
                'title' => $this->trans('ID',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'id_poi_type' => array(
                'title' => $this->trans('Types Infos Pratiques ',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'poi_name' => array(
                'title' => $this->trans('Nom Point d\'Interêt',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'poi_adresse' => array(
                'title' => $this->trans('Adresse Point d\'Interêt ',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'active' => array(
            	'title' => $this->trans('Enabled',array(), 'Admin.Global'),
            	'active' => 'status',
            	'type' => 'bool',
            	'align' => 'center',
            	'class' => 'fixed-width-xs',
            	'orderby' => false
            )  
        );
         /*Action modification et suppresion*/
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		/*parent:: construct();*/
	}
	//fin public construct
	public function renderForm()
	{
		
        //option select
            $poitype = ModelPoiType::getpoitype(true);
            $listepoitype = array();
            if(count($poitype) > 0){
                for ($i=0; $i < count($poitype) ; $i++) { 
                    $listepoitypeobject['key'] = $poitype[$i]['id_poi_type'];
                    $listepoitypeobject['name'] = $poitype[$i]['poi_type'];
                    array_push($listepoitype, $listepoitypeobject);
                }

            }
        //fin option select

        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('poi_liste', array(), 'Admin.Catalog.Feature'),
                'icon' => 'icon-certificate',
            ),
            'input' => array(
                
                  array(
                    'type' => 'select',
                    'label' => $this->trans('Types Infos Pratiques', array(), 'Admin.Global'),
                    'name' => 'id_poi_type',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
			            'query' => $listepoitype,
			            'id' => 'key',
			            'name' => 'name'
        			)
                ),
                 array(
                    'type' => 'text',
                    'label' => $this->trans('Nom Point d\'Interêt ', array(), 'Admin.Global'),
                    'name' => 'poi_name',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                 array(
                    'type' => 'text',
                    'label' => $this->trans('Adresse Point d\'Interêt ', array(), 'Admin.Global'),
                    'name' => 'poi_adresse',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
               
               
	             
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Enable', array(), 'Admin.Actions'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled', array(), 'Admin.Global'),
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled', array(), 'Admin.Global'),
                        ),
                    ),
                ),
            ),
        );
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save', array(), 'Admin.Actions'),
        );
         return parent::renderForm();
	}
	protected function l($string,$class = null,$addslashes = false,$htmlentities = true)
    {
    	if(_PS_VERSION_>='1.7'){
    		return Context::getContext()->getTranslator()->trans($string);
    	}else{
    		return parent::trans($string,$class,$addslashes,$htmlentities);
    	}

    }
}