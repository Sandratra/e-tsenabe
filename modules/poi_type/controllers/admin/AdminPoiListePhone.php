<?php

/**
 * 
 */
if(!class_exists( 'ModelPoiListePhone'));
   require_once _PS_MODULE_DIR_.'poi_type/classes/ModelPoiListePhone.php';

if(!class_exists( 'ModelPoiListe'));
   require_once _PS_MODULE_DIR_.'poi_type/classes/ModelPoiListe.php';
class AdminPoiListePhoneController extends ModuleAdminController
{
	public $bootstrap = true;
	public function __construct()
	{
		$this->table = 'poi_phone_liste';
		$this->list_id = 'poi_phone_liste';
		$this->className = 'ModelPoiListePhone';
		$this->lang = true;
		 $this->deleted = false;
		 $this->identifier = 'id_phone';

		  parent::__construct();
		    $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
            ),
        );

		     /* Liste d'affichage*/
		$this->fields_list = array(
			'id_phone' => array(
                'title' => $this->trans('ID',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'id_poi' => array(
                'title' => $this->trans('Point d\'interêt',array(), 'Admin.Global'),
                'align' => 'center',
            ),
            'id_poi_type_phone' => array(
                'title' => $this->trans('Type phone Point d\'interêt',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'poi_telephone' => array(
                'title' => $this->trans('Téléphone Point d\'interêt',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'active' => array(
            	'title' => $this->trans('Enabled',array(), 'Admin.Global'),
            	'active' => 'status',
            	'type' => 'bool',
            	'align' => 'center',
            	'class' => 'fixed-width-xs',
            	'orderby' => false
            )  
        );
         /*Action modification et suppresion*/
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		/*parent:: construct();*/
	}
	//fin public construct
	public function renderForm()
	{
        
        //option select	
        $poiliste = ModelPoiListe::getpoiliste(true);
        $listepoi = array();
        if(count($poiliste)){
            for ($i=0; $i < count($poiliste) ; $i++) { 
                $listepoiobject['key'] = $poiliste[$i]['id_poi'];
                $listepoiobject['name'] = $poiliste[$i]['poi_name'];
                array_push($listepoi , $listepoiobject);

            }
        }

        //fin option select
        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('Ajout phone Point d\'interêt', array(), 'Admin.Catalog.Feature'),
                'icon' => 'icon-certificate',
            ),
            'input' => array(
                  array(
                    'type' => 'select',
                    'label' => $this->trans('Point d\'interêt ', array(), 'Admin.Global'),
                    'name' => 'id_poi',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
			            'query' => $listepoi,
			            'id' => 'key',
			            'name' => 'name'
        			)
                ),
                  array(
                    'type' => 'select',
                    'label' => $this->trans('Type phone Point d\'interêt ', array(), 'Admin.Global'),
                    'name' => 'id_poi_type_phone',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
			            'query' => array(
			                array('key' => '1', 'name' => 'Fixe'),
			                array('key' => '2', 'name' => 'Portable'),
			                
			            ),
			            'id' => 'key',
			            'name' => 'name'
        			)
                ),
                 array(
                    'type' => 'text',
                    'label' => $this->trans('Téléphone Point d\'interêt', array(), 'Admin.Global'),
                    'name' => 'poi_telephone',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                
               
               
	             
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Enable', array(), 'Admin.Actions'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled', array(), 'Admin.Global'),
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled', array(), 'Admin.Global'),
                        ),
                    ),
                ),
            ),
        );
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save', array(), 'Admin.Actions'),
        );
         return parent::renderForm();
	}
	protected function l($string,$class = null,$addslashes = false,$htmlentities = true)
    {
    	if(_PS_VERSION_>='1.7'){
    		return Context::getContext()->getTranslator()->trans($string);
    	}else{
    		return parent::trans($string,$class,$addslashes,$htmlentities);
    	}

    }
}