{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<div class="categorie">
  {foreach  $categories item=category}
    <div class="catego" data-aos="zoom-in" >
      <img src="{{$category.image.large.url}}" alt="Avatar" class="imagecat">
      <div class="middle">
        <a href="{{$category.url}}"><div class="text">{{$category.name}}</div></a>
      </div>
    </div>
  {/foreach}
</div>
{* <div class="container-fluid" >
  <h2 class="h2 products-section-title text-uppercase" style="text-align:center;">Nos Catégories</h2>
  <div class="contents">
   {foreach  $categories item=category}
    <div class="card bg" data-aos="fade-up" style="background-image: url({{$category.image.large.url}});"> 
        <a href="{{$category.url}}"><h3>{{$category.name}}</h3></a>
    </div>
     
    {/foreach}
  </div>
</div>	 *}
<script>
$(document).ready(function()
 {
  var Lscreen=screen.width;
  var x=0;
  if(Lscreen > 992 ){
    x=6
  }else if(Lscreen < 992 &&  Lscreen>800){
    x=5
  }else if(Lscreen < 800 && Lscreen >775){
    x=4
  }else if(Lscreen < 775 && Lscreen >500){
    x=3
  }else if(Lscreen < 600){
    x=2
  }
  $("#tab").pagination({
  items: x,
  contents: 'categorie',
  previous: '<<',
  next: '>>',
  position: 'bottom',
  });
     
   
});
</script>
 

 
  


   
			


