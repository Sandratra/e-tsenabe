<?php


/**
 * 
 */
class ModelCategorieVente extends ObjectModel
{

	public $active = true;
	public $category;
	public $description;
	/**
     * @see ObjectModel::$definition
     */
	
	public static $definition =array(
		'table' => 'sale_category',
		'primary' => 'id_sale_category',
		'multilang' => true,
		'fields' => array(
			'category' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),

            'active' => array('type' =>self::TYPE_BOOL),
             /* Lang fields */
            'description' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
		),
	);

	 public static function getsalecategory($active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('sale_category')
    		->where ('active='.(int)$active);
            

    	return Db::getInstance() ->executeS($q); 
    }
}