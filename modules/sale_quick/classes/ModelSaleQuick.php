<?php


/**
 * 
 */
class ModelSaleQuick extends ObjectModel
{
	
	public $active = true;
	public $product_reference;
	public $real_price;
	public $temporary_price;
	public $start_date;
	public $end_date;
	public $sale_category_id;
	public $description;
	/**
     * @see ObjectModel::$definition
     */
	
	public static $definition =array(
		'table' => 'sale_quick',
		'primary' => 'id_sale_quick',
		'multilang' => true,
		'fields' => array(
			'product_reference' => array('type' => self::TYPE_HTML, 'required' => true),
			'real_price' => array('type' => self::TYPE_HTML, 'required' => true),
			'temporary_price' => array('type' => self::TYPE_HTML, 'required' => true),
			'start_date' => array('type' => self::TYPE_HTML, 'required' => true),
			'end_date' => array('type' => self::TYPE_HTML, 'required' => true),
			'sale_category_id' => array('type' => self::TYPE_HTML, 'required' => true),
            'active' => array('type' =>self::TYPE_BOOL),
             /* Lang fields */
            'description' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml'),
		),
	);

	public function getIdProductbyReference($reference){
        $q= new DbQuery();
        $q->select('id_product')
            ->from('product')
            ->where('reference='.'"'.$reference.'"');
            
        $instance= Db::getInstance() ->executeS($q);
        $temp = array();
        foreach ($instance as $value) {
          	$temp = $value;
        } 
        return  $temp['id_product'];

	}
	
	public function getCategoryProductbyReference($reference){
        $q= new DbQuery();
        $q->select('id_category_default')
            ->from('product')
            ->where('reference='.'"'.$reference.'"');
            
        $instance= Db::getInstance() ->executeS($q);
        $temp = array();
        foreach ($instance as $value) {
          	$temp = $value;
		}
		$q2= new DbQuery();
        $q2->select('link_rewrite')
            ->from('category_lang')
			->where('id_category='.$temp['id_category_default']);
		$instance2= Db::getInstance() ->executeS($q2);
		$temp2 = array();
		foreach ($instance2 as $value) {
				$temp2 = $value;
		}
        return $temp2['link_rewrite'] ;

    }


   /* public function getNameProductbyId($idproduct){
        $q= new DbQuery();
        $q->select('*')
            ->from('product_lang')
            ->where('reference='.'"'.$idproduct.'"');
            

        return Db::getInstance() ->executeS($q); 

	}*/
	public static $img_dir = _PS_IMG_DIR_.'salequick';
	

    public static function getImgPath($front=false){
        return  $front ? __PS_BASE_URI__.'img/salequick/' :__PS_BASE_URI__.'img/salequick/' ;
    }

	public static function getsalequick( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('sale_quick')
    		->where ('active='.(int)$active);
            

    	return Db::getInstance() ->executeS($q); 
    }
}