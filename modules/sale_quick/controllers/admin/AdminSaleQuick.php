<?php


/**
 * 
 */

if(!class_exists( 'ModelSaleQuick'));
   require_once _PS_MODULE_DIR_.'sale_quick/classes/ModelSaleQuick.php';
if(!class_exists( 'ModelCategorieVente'));
   require_once _PS_MODULE_DIR_.'sale_quick/classes/ModelCategorieVente.php';


// if(!class_exists( 'ModelSaleCategory'));
//    require_once _PS_MODULE_DIR_.'sale_category/classes/ModelSaleCategory.php';
class AdminSaleQuickController extends ModuleAdminController
{
	public $bootstrap = true;
	public function __construct()
	{
		$this->table = 'sale_quick';
		$this->list_id = 'sale_quick';
		$this->className = 'ModelSaleQuick';
		$this->lang = true;
		 $this->deleted = false;
		 $this->identifier = 'id_sale_quick';

		  parent::__construct();
		    $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
            ),
        );
        
        $this->fieldImageSettings = array(
            'name'=>'image',
            'dir' => 'salequick'
       );
		     /* Liste d'affichage*/
		$this->fields_list = array(
            'id_sale_quick' => array(
                'title' => $this->trans('ID',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'product_reference' => array(
                'title' => $this->trans('Référence des produits ',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'real_price' => array(
                'title' => $this->trans('Prix sur le marché',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'temporary_price' => array(
                'title' => $this->trans('Prix temporaire',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'start_date' => array(
                'title' => $this->trans('Date début',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'end_date' => array(
                'title' => $this->trans('Date fin',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'sale_category_id' => array(
                'title' => $this->trans('Id Catégorie Vente',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'active' => array(
            	'title' => $this->trans('Enabled',array(), 'Admin.Global'),
            	'active' => 'status',
            	'type' => 'bool',
            	'align' => 'center',
            	'class' => 'fixed-width-xs',
            	'orderby' => false
            )  
        );
         /*Action modification et suppresion*/
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		/*parent:: construct();*/
	}
	//fin public construct


    public static function getproduct()
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('product');
            

    	return Db::getInstance() ->executeS($q); 
    }

	//Formulaire d'ajout et de modification
	public function renderForm()
	{
       
		if (!($salequick = $this->loadObject(true))) {
            return;
        }
        $image = ModelSalequick::$img_dir.'/'.$salequick->id . '.jpg';
        $this->imageType = 'jpg';
        $image_url = ImageManager::thumbnail(
            $image,
            $this->table . '_' . (int) $salequick->id . '.' . $this->imageType ,
            350,
            $this->imageType,
            true,
            true
        );
        
        $image_size = file_exists($image) ? filesize($image) / 1000 : false;

         //option select
        $categorievente = ModelCategorieVente::getsalecategory(true);
       /* var_dump($categorievente);*/
        $listCateg = array();
        if (count($categorievente) > 0) {
            for ($i = 0 ; $i < count($categorievente) ; $i++) {
                $listCategObject['key'] = $categorievente[$i]['id_sale_category'];
                $listCategObject['name'] = $categorievente[$i]['category'];
                array_push($listCateg, $listCategObject);
            }
        }

        //fin option select
       
        $this->fields_form = array(
             
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('Ajout produit aux ventes flashs ou bonnes affaires', array(), 'Admin.Catalog.Feature'),
                'icon' => 'icon-certificate',
            ),
            'input' => array(
                
                array(
                    'type' => 'text',
                    'label' => $this->trans('Référence du produit', array(), 'Admin.Global'),
                    'name' => 'product_reference',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Prix sur le marché', array(), 'Admin.Global'),
                    'name' => 'real_price',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->trans('Prix tempotaire', array(), 'Admin.Global'),
                    'name' => 'temporary_price',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                array(
                    'type' => 'datetime',
                    'label' => $this->trans('Date début', array(), 'Admin.Global'),
                    'name' => 'start_date',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                 array(
                    'type' => 'datetime',
                    'label' => $this->trans('Date fin', array(), 'Admin.Global'),
                    'name' => 'end_date',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),

                array(
                    'type' => 'select',
                    'label' => $this->trans('Catégorie Vente', array(), 'Admin.Global'),
                    'name' => 'sale_category_id',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
			            'query' => $listCateg,
			            'id' => 'key',
			            'name' => 'name'
        			)
                ),
                array(
                    'type' => 'file',
                    'label' => $this->trans('Image', array(), 'Admin.Global'),
                    'name' => 'image',
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'display_image' => true,
                    'col' => 6,
                    'hint' => $this->trans('Upload a brand logo from your computer.', array(), 'Admin.Catalog.Help'),
                ),
              
	            array(
	                    'type' => 'textarea',
	                    'label' => $this->trans('Description', array(), 'Admin.Global'),
	                    'name' => 'description',
	                    'lang' => true,
	                    'cols' => 60,
	                    'rows' => 10,
	                    'col' => 6,
	                    'autoload_rte' => 'rte', //Enable TinyMCE editor for description
	                    'hint' => $this->l('Invalid characters:', array(), 'Admin.Notifications.Info') . ' &lt;&gt;;=#{}',
	                ),
	               
              
              
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Enable', array(), 'Admin.Actions'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled', array(), 'Admin.Global'),
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled', array(), 'Admin.Global'),
                        ),
                    ),
                ),
            ),
        );
        if (!($salequick = $this->loadObject(true))) {
            return;
        }
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save', array(), 'Admin.Actions'),
        );
         return parent::renderForm();
	}

	 protected function l($string,$class = null,$addslashes = false,$htmlentities = true)
    {
    	if(_PS_VERSION_>='1.7'){
    		return Context::getContext()->getTranslator()->trans($string);
    	}else{
    		return parent::trans($string,$class,$addslashes,$htmlentities);
    	}

    }
}