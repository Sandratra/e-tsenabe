<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'sale_quick` (
    `id_sale_quick` int(11) NOT NULL AUTO_INCREMENT,
     `active` int(1) default 0,
     `product_reference` varchar(50) NOT NULL,
     `real_price` DOUBLE(19,2),
     `temporary_price` DOUBLE(19,2),
     `start_date` DATETIME,
     `end_date` DATETIME,
     `creation_date` DATETIME,
     `sale_category_id` int(11) NOT NULL,
    PRIMARY KEY  (`id_sale_quick`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';


$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'sale_quick_lang` (
    `id_sale_quick` int(11) NOT NULL,
    `id_lang` int(11) UNSIGNED NOT NULL,
    `description` text,
    PRIMARY KEY  (`id_sale_quick`,`id_lang`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';


foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
