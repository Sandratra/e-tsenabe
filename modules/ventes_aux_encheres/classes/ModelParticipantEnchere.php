<?php

/**
 * 
 */
class ModelParticipantEnchere extends ObjectModel
{
	
	public $active = true;
	public $id_customer;
	public $id_produit_enchere;
	public $statut_participant;
	/**
     * @see ObjectModel::$definition
     */

	public static $definition =array(
		'table' => 'liste_participant_ventes_encheres',
		'primary' => 'id_participant',
		'multilang' => true,
		'fields' => array(
			'id_customer' => array('type' => self::TYPE_HTML, 'required' => true),
			'id_produit_enchere' => array('type' => self::TYPE_HTML, 'required' => true),
			'statut_participant' => array('type' => self::TYPE_STRING, 'required' => true, 'size' => 255),
            'active' => array('type' =>self::TYPE_BOOL),
             
		),
	);
    
    public static function getclient( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('customer')
    		->where ( ('id_shop !=' .'1') );
            

    	return Db::getInstance() ->executeS($q); 
    }
     public static function getparticipant( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('liste_participant_ventes_encheres')
    		->where ('active='.(int)$active);
            

    	return Db::getInstance() ->executeS($q); 
    }
    
   

}