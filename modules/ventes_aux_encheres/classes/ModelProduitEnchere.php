<?php

/**
 * 
 */
class ModelProduitEnchere extends ObjectModel
{
	public $active = true;
	public $ref_produit;
	public $date_debut_enchere;
	public $date_fin_enchere;
	public $id_employee;
	public $prix_debut_enchere;
	public $lien_page;
	/**
     * @see ObjectModel::$definition
     */

	public static $definition =array(
		'table' => 'liste_produit_ventes_encheres',
		'primary' => 'id_produit_enchere',
		'multilang' => true,
		'fields' => array(
			'ref_produit' => array('type' => self::TYPE_HTML, 'required' => true),
			'date_debut_enchere' => array('type' => self::TYPE_HTML, 'required' => true),
			'date_fin_enchere' => array('type' => self::TYPE_HTML, 'required' => true),
			'id_employee' => array('type' => self::TYPE_HTML),
			'prix_debut_enchere' => array('type' => self::TYPE_HTML, 'required' => true),
			'lien_page' => array('type' => self::TYPE_HTML),
            'active' => array('type' =>self::TYPE_BOOL),
             
		),
	);

	public static $img_dir = _PS_IMG_DIR_.'ventesencheres';
	public static function getImgPath($front=false){
        return  $front ? __PS_BASE_URI__.'img/ventesencheres/' :__PS_BASE_URI__.'img/ventesencheres/' ;
    }

	 public static function getproduitenchere( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('liste_produit_ventes_encheres')
    		->where ('active='.(int)$active);
    	return Db::getInstance() ->executeS($q); 
    }
     public static function getemployee( $active=true)
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('employee')
    		->where ('active='.(int)$active);
            

    	return Db::getInstance() ->executeS($q); 
    }

   public function getIdProductbyReference($reference){
        $q= new DbQuery();
        $q->select('id_product')
            ->from('product')
            ->where('reference='.'"'.$reference.'"');
            
        $instance= Db::getInstance() ->executeS($q);
        $temp = array();
        foreach ($instance as $value) {
          	$temp = $value;
        } 
        return  $temp['id_product'];

	}
}