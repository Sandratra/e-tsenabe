<?php 

/**
 * 
 */
class ModelVenteEnchere extends ObjectModel
{
	
	public $active = true;
	public $id_produit_enchere;
	public $id_participant;
	public $prix_client;
	

	/**
     * @see ObjectModel::$definition
     */

	public static $definition =array(
		'table' => 'ventes_aux_encheres',
		'primary' => 'id_ventes_aux_encheres',
		'multilang' => true,
		'fields' => array(
			'id_produit_enchere' => array('type' => self::TYPE_HTML, 'required' => true),
			'id_participant' => array('type' => self::TYPE_HTML, 'required' => true),
			'prix_client' => array('type' => self::TYPE_HTML, 'required' => true),
            'active' => array('type' =>self::TYPE_BOOL),
             
		),
	);
	 public static function getproduct()
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('product');
            

    	return Db::getInstance() ->executeS($q); 
    }

     public static function getnameproduct()
    {
    	$q= new DbQuery();
    	$q->select('*')
    		->from('product_lang');
            

    	return Db::getInstance() ->executeS($q); 
    }
}