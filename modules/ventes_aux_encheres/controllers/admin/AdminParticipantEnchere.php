<?php

/**
 * 
 */


if(!class_exists( 'ModelParticipantEnchere'));
   require_once _PS_MODULE_DIR_.'ventes_aux_encheres/classes/ModelParticipantEnchere.php';

if(!class_exists( 'ModelProduitEnchere'));
   require_once _PS_MODULE_DIR_.'ventes_aux_encheres/classes/ModelProduitEnchere.php';   

class AdminParticipantEnchereController extends ModuleAdminController
{
	public $bootstrap = true;
	public function __construct()
	{
		$this->table = 'liste_participant_ventes_encheres';
		$this->list_id = 'liste_participant_ventes_encheres';
		$this->className = 'ModelParticipantEnchere';
		$this->lang = true;
		 $this->deleted = false;
		 $this->identifier = 'id_participant';

		  parent::__construct();
		    $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
            ),
        );

        
		     /* Liste d'affichage*/
		$this->fields_list = array(
            'id_participant' => array(
                'title' => $this->trans('ID',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'id_customer' => array(
                'title' => $this->trans('id Client',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'id_produit_enchere' => array(
                'title' => $this->trans('id produit enchere',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'statut_participant' => array(
                'title' => $this->trans('Statut Participant',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'active' => array(
            	'title' => $this->trans('Enabled',array(), 'Admin.Global'),
            	'active' => 'status',
            	'type' => 'bool',
            	'align' => 'center',
            	'class' => 'fixed-width-xs',
            	'orderby' => false
            )  
        );
         /*Action modification et suppresion*/
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		/*parent:: construct();*/
	}
	//fin public construct
    
	
    //Formulaire d'ajout et de modification
    public function renderForm()
    {
       
        $client = ModelParticipantEnchere::getclient(true);
         //option select
        $listClient = array();
        if (count($client) > 0) {
            for ($i = 0 ; $i < count($client) ; $i++) {
                $listClientObject['key'] = $client[$i]['id_customer'];
                $listClientObject['name'] = $client[$i]['firstname'];
                array_push($listClient, $listClientObject);
            }
        }

        $produitenchere = ModelProduitEnchere::getproduitenchere(true);
        $listProduitEnchere = array();
        if (count($produitenchere) > 0) {
            for ($i = 0 ; $i < count($produitenchere) ; $i++) {
                $listProduitObject['key'] = $produitenchere[$i]['id_produit_enchere'];
                $listProduitObject['name'] = $produitenchere[$i]['ref_produit'];
                array_push($listProduitEnchere, $listProduitObject);
            }
        }

       

        //fin option select

        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('liste_participant_ventes_encheres', array(), 'Admin.Catalog.Feature'),
                'icon' => 'icon-certificate',
            ),
            'input' => array(
              
             
              
                array(
                    'type' => 'select',
                    'label' => $this->trans('Client ', array(), 'Admin.Global'),
                    'name' => 'id_customer',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
                        'query' => $listClient,
                        'id' => 'key',
                        'name' => 'name'
                    )
                ),
                
                 
                array(
                    'type' => 'select',
                    'label' => $this->trans('id Produit enchere ', array(), 'Admin.Global'),
                    'name' => 'id_produit_enchere',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
                        'query' => $listProduitEnchere,
                        'id' => 'key',
                        'name' => 'name'
                    )
                ),
                   array(
                    'type' => 'select',
                    'label' => $this->trans('Statut ', array(), 'Admin.Global'),
                    'name' => 'statut_participant',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
                        'query' => array(
                            array('key' => 'Pending', 'name' => 'Pending'),
                            array('key' => 'Confirmed', 'name' => 'Confirmed'),
                            array('key' => 'Bl', 'name' => 'Blocked'),
                            
                        ),
                        'id' => 'key',
                        'name' => 'name'
                    )
                ),
               
                 
               /* array(
                    'type' => 'datetime',
                    'label' => $this->trans('Creation date', array(), 'Admin.Global'),
                    'name' => 'creation_date',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),*/

             
            
                   
              
              
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Enable', array(), 'Admin.Actions'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled', array(), 'Admin.Global'),
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled', array(), 'Admin.Global'),
                        ),
                    ),
                ),
            ),
        );
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save', array(), 'Admin.Actions'),
        );
         return parent::renderForm();
    }

     protected function l($string,$class = null,$addslashes = false,$htmlentities = true)
    {
        if(_PS_VERSION_>='1.7'){
            return Context::getContext()->getTranslator()->trans($string);
        }else{
            return parent::trans($string,$class,$addslashes,$htmlentities);
        }

    }
}