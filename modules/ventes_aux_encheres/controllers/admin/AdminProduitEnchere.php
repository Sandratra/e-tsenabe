<?php

/**
 * 
 */

if(!class_exists( 'ModelProduitEnchere'));
   require_once _PS_MODULE_DIR_.'ventes_aux_encheres/classes/ModelProduitEnchere.php';
class AdminProduitEnchereController extends ModuleAdminController
{
	public $bootstrap = true;
	public function __construct()
	{
		$this->table = 'liste_produit_ventes_encheres';
		$this->list_id = 'liste_produit_ventes_encheres';
		$this->className = 'ModelProduitEnchere';
		$this->lang = true;
		 $this->deleted = false;
		 $this->identifier = 'id_produit_enchere';

		  parent::__construct();
		    $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
            ),
        );

        $this->fieldImageSettings = array(
            'name'=>'image',
            'dir' => 'ventesencheres'
       );
        
		     /* Liste d'affichage*/
		$this->fields_list = array(
            'id_produit_enchere' => array(
                'title' => $this->trans('ID',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'ref_produit' => array(
                'title' => $this->trans('Reference Produit',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'date_debut_enchere' => array(
                'title' => $this->trans('Date debut',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'date_fin_enchere' => array(
                'title' => $this->trans('Date fin',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
             'date_creation_enchere' => array(
                'title' => $this->trans('Date Creation',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
              'id_employee' => array(
                'title' => $this->trans('Responsable',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
               'prix_debut_enchere' => array(
                'title' => $this->trans('Prix debut',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
                'lien_page' => array(
                'title' => $this->trans('Lien',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'active' => array(
            	'title' => $this->trans('Enabled',array(), 'Admin.Global'),
            	'active' => 'status',
            	'type' => 'bool',
            	'align' => 'center',
            	'class' => 'fixed-width-xs',
            	'orderby' => false
            )  
        );
         /*Action modification et suppresion*/
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		/*parent:: construct();*/
	}
	//fin public construct
     //Formulaire d'ajout et de modification
    public function renderForm()
    {
       $this->addJS(_PS_JS_DIR_ . 'admin/style.js');
       if (!($ventesencheres = $this->loadObject(true))) {
        return;
       }
       $image = ModelProduitEnchere::$img_dir.'/'.$ventesencheres->id . '.jpg';
       /*var_dump($image);*/
       $image_url = ImageManager::thumbnail(
           $image,
           $this->table . '_' . (int) $ventesencheres->id . '.' . $this->imageType,
           350,
           $this->imageType,
           true,
           true
       );
       $image_size = file_exists($image) ? filesize($image) / 1000 : false;

        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('Ajout produit enchere ', array(), 'Admin.Catalog.Feature'),
                'icon' => 'icon-certificate',
            ),
            'input' => array(              
                array(
                    'type' => 'text',
                    'label' => $this->trans(' reference produit', array(), 'Admin.Global'),
                    'name' => 'ref_produit',
                    'id' => 'product_reference',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',   
                ),    
                  array(
                    'type' => 'text',
                    'label' => $this->trans('Info', array(), 'Admin.Global'),
                    'name' => 'infoInput',
                    'id' => 'infoInput',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),        
              
                array(
                    'type' => 'datetime',
                    'label' => $this->trans(' date debut', array(), 'Admin.Global'),
                    'name' => 'date_debut_enchere',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'id'=>'clicker'
                ),
                 array(
                    'type' => 'datetime',
                    'label' => $this->trans(' date fin', array(), 'Admin.Global'),
                    'name' => 'date_fin_enchere',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                   
                array(
                    'type' => 'text',
                    'label' => $this->trans(' Prix initial', array(), 'Admin.Global'),
                    'name' => 'prix_debut_enchere',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),
                 array(
                    'type' => 'hidden',
                    'name' => 'id_employee',
                ),
                array(
                    'type' => 'hidden',
                    'name' => 'lien_page',
                ),
                array(
                    'type' => 'file',
                    'label' => $this->trans('Image', array(), 'Admin.Global'),
                    'name' => 'image',
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'display_image' => true,
                    'col' => 6,
                    'hint' => $this->trans('Upload a brand logo from your computer.', array(), 'Admin.Catalog.Help'),
                ),
                      
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Enable', array(), 'Admin.Actions'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled', array(), 'Admin.Global'),
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled', array(), 'Admin.Global'),
                        ),
                    ),
                ),
            ),
        );
        if (!($ventesencheres = $this->loadObject(true))) {
            return;
        }
         // var_dump($_POST);
        // if(isset($_POST['valeur'])){
        //     $valeur=$_POST['valeur'];       
        // }
        $valeur="BG-005";
        //get id
        $id = ModelProduitEnchere::getIdProductbyReference($valeur);
        //get url
        $url=$this->context->link->getProductLink($id,0 ,0 ,0 ,0 ,2);
        global $cookie;
        $id1=$cookie->id_employee;
        $this->fields_value = array('lien_page'=>$url ,'id_employee'=>$id1);
        $this->fields_form['submit'] = array(
        'title' => $this->l('Save', array(), 'Admin.Actions'),
        );
        return parent::renderForm();
    }

     protected function l($string,$class = null,$addslashes = false,$htmlentities = true)
    {
        if(_PS_VERSION_>='1.7'){
            return Context::getContext()->getTranslator()->trans($string);
        }else{
            return parent::trans($string,$class,$addslashes,$htmlentities);
        }

    }
	
	
}