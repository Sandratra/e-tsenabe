<?php

/**
 * 
 */

if(!class_exists( 'ModelParticipantEnchere'));
   require_once _PS_MODULE_DIR_.'ventes_aux_encheres/classes/ModelParticipantEnchere.php';

if(!class_exists( 'ModelProduitEnchere'));
   require_once _PS_MODULE_DIR_.'ventes_aux_encheres/classes/ModelProduitEnchere.php';   
if(!class_exists( 'ModelVenteEnchere'));
   require_once _PS_MODULE_DIR_.'ventes_aux_encheres/classes/ModelVenteEnchere.php';   

class AdminVenteEnchereController extends ModuleAdminController
{
	public $bootstrap = true;
	public function __construct()
	{
		$this->table = 'ventes_aux_encheres';
		$this->list_id = 'ventes_aux_encheres';
		$this->className = 'ModelVenteEnchere';
		$this->lang = true;
		 $this->deleted = false;
		 $this->identifier = 'id_ventes_aux_encheres';

		  parent::__construct();
		    $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->trans('Delete selected', array(), 'Admin.Actions'),
                'icon' => 'icon-trash',
                'confirm' => $this->trans('Delete selected items?', array(), 'Admin.Notifications.Warning'),
            ),
        );
        
		     /* Liste d'affichage*/
		$this->fields_list = array(
            'id_ventes_aux_encheres' => array(
                'title' => $this->trans('ID',array(), 'Admin.Global'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
            ),
            'id_produit_enchere' => array(
                'title' => $this->trans(' Produit Enchere',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'id_participant' => array(
                'title' => $this->trans('Client Participant',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
            'prix_client' => array(
                'title' => $this->trans('Prix Client',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
             'date_creation' => array(
                'title' => $this->trans('Date Creation',array(), 'Admin.Global'),
                'width' => 'auto',
            ),
              
            'active' => array(
            	'title' => $this->trans('Enabled',array(), 'Admin.Global'),
            	'active' => 'status',
            	'type' => 'bool',
            	'align' => 'center',
            	'class' => 'fixed-width-xs',
            	'orderby' => false
            )  
        );
         /*Action modification et suppresion*/
        $this->addRowAction('edit');
        $this->addRowAction('delete');
		/*parent:: construct();*/
	}
	//fin public construct
    //Formulaire d'ajout et de modification
    public function renderForm()
    {      
         //option select
        $product = ModelVenteEnchere::getproduct();
        $productlang = ModelVenteEnchere::getnameproduct();
        $client = ModelParticipantEnchere::getclient(true);
        $participant = ModelParticipantEnchere::getparticipant(true);
        $produitenchere = ModelProduitEnchere::getproduitenchere(true);
        //select id participant
        $listeEnsemble = array();
        if((count($participant) > 0) && (count($client) > 0)){
            for($i = 0 ; $i< count($participant) ; $i++){
                for ($j = 0; $j < count($client) ; $j++) { 
                   if( $participant[$i]['id_customer'] == $client[$j]['id_customer']){
                      $listeEnsembleObject['key'] = $participant[$i]['id_participant'];
                      $listeEnsembleObject['name'] = $client[$j]['firstname'];
                      array_push($listeEnsemble, $listeEnsembleObject);
                      break;
                   }
                }//end for j
            }//end for i
        }//end if
        //select produit enchere
         $listeproduit = array();
         if( (count($produitenchere)>0) && (count($product)>0) &&(count($productlang))){
            for($i = 0 ; $i< count($produitenchere) ; $i++){
                for($j = 0; $j < count($product) ; $j++){
                    if($produitenchere[$i]['ref_produit']==$product[$j]['reference']){
                        for($l = 0; $l < count($productlang) ; $l++){
                            if($product[$j]['id_product']==$productlang[$l]['id_product']){
                                $listeObjectProduct['key']=$produitenchere[$i]
                                                           ['id_produit_enchere'];
                                $listeObjectProduct['name']=$productlang[$l]['name'];
                                 array_push($listeproduit, $listeObjectProduct);
                                break;
                            }   
                        }//end for l
                        break;
                    }
                }//end for j
            }//end for i
         }//end if

        //fin option select

        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->trans('ventes_aux_encheres', array(), 'Admin.Catalog.Feature'),
                'icon' => 'icon-certificate',
            ),
            'input' => array(
                  
                array(
                    'type' => 'select',
                    'label' => $this->trans(' Produit enchere', array(), 'Admin.Global'),
                    'name' => 'id_produit_enchere',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
                        'query' => $listeproduit,
                        'id' => 'key',
                        'name' => 'name'
                    )
                ),
                   array(
                    'type' => 'select',
                    'label' => $this->trans('Participant ', array(), 'Admin.Global'),
                    'name' => 'id_participant',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                    'options' => array(
                        'query' => $listeEnsemble,
                        'id' => 'key',
                        'name' => 'name'
                    )
                ),
               
                 
                array(
                    'type' => 'text',
                    'label' => $this->trans(' Prix Client', array(), 'Admin.Global'),
                    'name' => 'prix_client',
                    'col' => 4,
                    'required' => true,
                    'hint' => $this->trans('Invalid characters:') . ' &lt;&gt;;=#{}',
                ),         
              
                array(
                    'type' => 'switch',
                    'label' => $this->trans('Enable', array(), 'Admin.Actions'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled', array(), 'Admin.Global'),
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled', array(), 'Admin.Global'),
                        ),
                    ),
                ),
            ),
        );
        $this->fields_form['submit'] = array(
            'title' => $this->l('Save', array(), 'Admin.Actions'),
        );
         return parent::renderForm();
    }

     protected function l($string,$class = null,$addslashes = false,$htmlentities = true)
    {
        if(_PS_VERSION_>='1.7'){
            return Context::getContext()->getTranslator()->trans($string);
        }else{
            return parent::trans($string,$class,$addslashes,$htmlentities);
        }

    }
}