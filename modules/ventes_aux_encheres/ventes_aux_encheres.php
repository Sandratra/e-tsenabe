<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

if(!class_exists( 'ModelProduitEnchere'));
   require_once _PS_MODULE_DIR_.'ventes_aux_encheres/classes/ModelProduitEnchere.php';

class Ventes_aux_encheres extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'ventes_aux_encheres';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Crea-Tic';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();


         $this->tabs = array(
            array('name' => 'Ventes Aux Encheres' , 'class_name' => 'VentesEncheres', 
                    'parent'=>''  ),
             array('name' => 'Liste des participants aux ventes','class_name' => 'AdminParticipantEnchere',
                    'parent'=>'VentesEncheres'),
            array('name' => 'Liste des produits aux ventes ','class_name' => 'AdminProduitEnchere',
                    'parent'=>'VentesEncheres'),
            array('name' => 'Liste des ventes aux encheres','class_name' => 'AdminVenteEnchere',
                    'parent'=>'VentesEncheres'),
        );
        // $this->installTab(true);
        //  $this->installFolder();
        $this->displayName = $this->l('Ventes aux encheres');
        $this->description = $this->l('Gestion des Ventes aux encheres');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
       /* Configuration::updateValue('VENTES_AUX_ENCHERES_LIVE_MODE', false);*/
         require _PS_MODULE_DIR_.'ventes_aux_encheres/sql/install.php';
        return parent::install() &&
            $this->installTab() &&
            $this->registerHook('header') &&
            $this->registerHook('displayHome') &&
            $this->registerHook('backOfficeHeader');
    }

    public function uninstall()
    {
        /*Configuration::deleteByName('VENTES_AUX_ENCHERES_LIVE_MODE');*/
         require _PS_MODULE_DIR_.'ventes_aux_encheres/sql/uninstall.php';
        return parent::uninstall();
    }


 //installation de l'onglet dans admin

    public function installTab($install = true){
        if($install){
            $languages = Language::getLanguages();
            foreach ($this->tabs as $t) {
                $tab = new Tab();
                $tab->module = $this->name;
                $tab->class_name = $t['class_name']; 
                $tab->id_parent = Tab::getIdFromClassName($t['parent']);
                foreach ($languages as $language) {
                    $tab->name[$language['id_lang']] = $t['name']; 
                }   
                 $tab->save(); 
            }
            return true;
        }else{
            foreach($this->tabs as $t){
                $id = Tab::getIdFromClassName($t['class_name']);   
                 if($id){
                    $tab = new Tab($id);
                     $tab->delete();
                 } 
            }
            return true;           
        }
    }

    public function installFolder(){
        if(!file_exists(ModelProduitEnchere::$img_dir))
            return  mkdir(ModelProduitEnchere::$img_dir , '0777');
        return true;
    }
    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitVentes_aux_encheresModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitVentes_aux_encheresModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'VENTES_AUX_ENCHERES_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'VENTES_AUX_ENCHERES_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' => 'password',
                        'name' => 'VENTES_AUX_ENCHERES_ACCOUNT_PASSWORD',
                        'label' => $this->l('Password'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'VENTES_AUX_ENCHERES_LIVE_MODE' => Configuration::get('VENTES_AUX_ENCHERES_LIVE_MODE', true),
            'VENTES_AUX_ENCHERES_ACCOUNT_EMAIL' => Configuration::get('VENTES_AUX_ENCHERES_ACCOUNT_EMAIL', 'contact@prestashop.com'),
            'VENTES_AUX_ENCHERES_ACCOUNT_PASSWORD' => Configuration::get('VENTES_AUX_ENCHERES_ACCOUNT_PASSWORD', null),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayHome()
    {
       $ventes_encheres = ModelProduitEnchere::getproduitenchere(true);
       $vente = array();    
       foreach ($ventes_encheres as $enchere) {   
        $id_product =ModelProduitEnchere::getIdProductbyReference($enchere['ref_produit']);
        $vente[] = array(
            'id' => $id_product,
            'id_produit_enchere' => $enchere['id_produit_enchere'],
            'date_debut_enchere' => $enchere['date_debut_enchere'],
            'vente_path' => ModelProduitEnchere::getImgPath(true),
            'date_fin_enchere' => $enchere['date_fin_enchere'],
            'prix_debut_enchere' => $enchere['prix_debut_enchere'],
            'url' => $this->context->link->getProductLink($id_product,0,0,0,0,2)
        );
    }
    $this->context->smarty->assign(array(
        'ventesauxencheres'=>$vente,
    ));
        
        return $this->display(__FILE__,'ventes_encheres.tpl');
    }
}
