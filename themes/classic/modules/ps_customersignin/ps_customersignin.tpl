{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div id="_desktop_user_info" style="display:inline-block;">
  <div class="user-info">
    {if $logged}
    <div class="dropdown js-dropdown">
      <button data-toggle="dropdown" class="btn-unstyle" aria-haspopup="true" aria-expanded="false">
        <a class="account hidden-sm-down" href="{$my_account_url}" title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}" rel="nofollow"><i class="material-icons">&#xE7FF;</i> {l s='Your account' d='Shop.Theme.Customeraccount'}</a> 
        <a class="account hidden-md-up" href="{$my_account_url}" title="{l s='View my customer account' d='Shop.Theme.Customeraccount'}" rel="nofollow"><i class="material-icons">&#xE7FF;</i></a> 
        <i class="material-icons expand-more"></i>
      </button>
      <ul class="dropdown-menu">
        <li class="current">
          <a class="logout hidden-sm-down dropdown-item" href="{$logout_url}" rel="nofollow">{l s='Sign out' d='Shop.Theme.Actions'}</a>
        </li>
      </ul>
     {* <select class="link hidden-md-up" aria-labelledby="language-selector-label">

        <option value="{$my_account_url}" selected="selected" data-iso-code="fr">
          <span><i class="material-icons">&#xE7FF;</i>
          {l s='Your account' d='Shop.Theme.Customeraccount'}</span>
        </option>
        <option value="{$logout_url}" data-iso-code="en">
          {l s='Sign out' d='Shop.Theme.Actions'}
        </option>
      </select> *}
    </div>
    {* <ul>
       <li>
        
        <button data-toggle="dropdown" class="dropbtn"><i class="material-icons expand-more"></i></button>
        <div id="myDropdown" class="dropdown-content">
          
        </div>
      </li>
    </ul> *}
        {* <li>  
          <a
            onclick="myFunction()" 
            class="account  dropbtn"
            href="{$my_account_url}"
            title="{l s='Your account' d='Shop.Theme.Customeraccount'}"
            rel="nofollow"
            >
          <div id="myDropdown" class="dropdown-content">
          <a
            class="logout hidden-sm-down"
            href="{$logout_url}"
            rel="nofollow"
          >
             <i class="material-icons">&#xE7FF;</i> 
            <span class="material-icons" style="font-size: 30px;">account_box</span>
            {l s='Sign out' d='Shop.Theme.Actions'}
          </a>
          </div>
               <i class="material-icons hidden-md-up logged">&#xE7FF;</i>
              <span class="hidden-sm-down"></span> 
              
        </li>
     </ul>  *}
      
      
    {else}
      <a
        href="{$my_account_url}"
        title="{l s='Log in to your customer account' d='Shop.Theme.Customeraccount'}"
        rel="nofollow"
      >
        <i class="material-icons">&#xE7FF;</i>
        <span class="hidden-sm-down">{l s='Sign in' d='Shop.Theme.Actions'}</span>
      </a>
    {/if}
  </div>
</div>
