{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{* {block name='header_banner'}
  <div class="header-banner">
    {hook h='displayBanner'}
  </div>
{/block} *}

{* {block name='header_nav'}
  <nav class="header-nav">
    <div class="container">
      <div class="row">
        <div class="hidden-sm-down">
          <div class="col-md-5 col-xs-12">
            {hook h='displayNav1'}
          </div>
          <div class="col-md-7 right-nav">
              
          </div>
        </div>
        <div class="hidden-md-up text-sm-center mobile">
          <div class="float-xs-left" id="menu-icon">
            <i class="material-icons d-inline">&#xE5D2;</i>
          </div>
          <div class="float-xs-right" id="_mobile_cart"></div>
          <div class="float-xs-right" id="_mobile_user_info"></div>
          <div class="top-logo" id="_mobile_logo"></div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </nav>
{/block} *}

{block name='header_top'}
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4" id="_desktop_logo">
          <h1 >
            <a href="{$urls.base_url}" >
              <img style="margin-top: 5px;" class="logo img-responsive animate__animated animate__tada animate__slow" src="{$shop.logo}" alt="{$shop.name}" >
            </a>
          </h1>
        </div>
        <div class="col-lg-5 col-md-4 col-sm-4 hidden-sm-down  position-static">
          {hook h='displaySearch'}
          <div class="clearfix"></div>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-9 col-xs-8 connection">
          {hook h='displayNav2'}
        </div>
      </div>
      <div class="col-xs-12 hidden-md-up position-static" style="margin-top: 5px;">
        {hook h='displaySearch'}
        <div class="clearfix"></div>
      </div>
      
      {* <div id="mobile_top_menu_wrapper" class="row hidden-md-up" style="display:none;">
        <div class="js-top-menu mobile" id="_mobile_top_menu"></div>
        <div class="js-top-menu-bottom">
          <div id="_mobile_currency_selector"></div>
          <div id="_mobile_language_selector"></div>
          <div id="_mobile_contact_link"></div>
        </div>
      </div> *}
    </div>
  </div>
  {hook h='displayNavFullWidth'}
{/block}
{* block menu *}
{block name='header_top'}
  <div class="header-top" style="padding-top:10px;">
    <div class="container">
      <div class="row">
        <div class=" col-md-12 col-sm-12 position-static menu-bottom">
          <div class="menu">
             <ul class="category-top-menu">
              <li data-depth="0" style="margin-right:-15px;">
                <span class="material-icons icon-home" style="margin-bottom:5px;">home</span>
              </li>
              <li data-depth="0" >
                <a class="cat category-link" href="{$urls.base_url}" >
                Accueil
                </a>
              </li>
              <li data-depth="0">
                <a  class="cat category-link" href="{$urls.about_us}" title="A propos de nous">
                Qui sommes nous?
                </a>
              </li>
              <li data-depth="0">
                <a class="cat category-link" href="{$urls.pages.contact}" title="Utiliser le formulaire pour nous contacter">
                Contactez-nous
                </a>
              </li>
            </ul>
          </div>
          {hook h='displayTop'}
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
{/block}
{* fin block *}
