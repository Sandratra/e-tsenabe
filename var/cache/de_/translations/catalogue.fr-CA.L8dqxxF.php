<?php

use Symfony\Component\Translation\MessageCatalogue;

$catalogue = new MessageCatalogue('fr-CA', array (
  'AdminActions' => 
  array (
    'Uninstall' => 'Désinstaller',
    'Choose a file' => 'Choisissez un fichier',
    'Validate' => 'Valider',
    'Generate' => 'Générer',
    'Select all' => 'Tout sélectionner',
    'Unselect all' => 'Tous désélectionner',
    'Display' => 'Affichage',
    '-- Choose --' => 'Choisissez',
    'View' => 'Afficher',
    'Upload' => 'Mettre en ligne',
    'Import' => 'Importer',
    'Close' => 'Fermer',
    'Disable' => 'Désactiver',
    'Configure' => 'Configurer',
    'Translate' => 'Traduire',
    'Filter' => 'Filtrer',
    'Show' => 'Montrer',
    'Update' => 'Mettre à jour',
    'Edit' => 'Modifier',
    'Choose' => 'Choisissez',
    'Shipping' => 'Expédition',
    'Print out' => 'Imprimer',
    'Preview' => 'Visualiser',
    'Remove' => 'Retirer',
    'Reset' => 'Réinitialiser',
    'Save and stay' => 'Enregistrer et rester',
    'and stay' => 'et rester',
    'Choose language:' => 'Choisissez la langue :',
    'Cancel' => 'Annuler',
    'Apply' => 'Enregistrer',
    'Add' => 'Ajouter',
    'Search' => 'Rechercher',
    'Save' => 'Enregistrer',
    'Delete' => 'Supprimer',
    'Collapse All' => 'Réduire tout',
    'Expand All' => 'Ouvrir tout',
    'Check All' => 'Tout cocher',
    'Uncheck All' => 'Tout décocher',
    'Find a category' => 'Trouver une catégorie',
    'Finish' => 'Terminer',
    'Add New' => 'Ajouter',
    'Delete selected items?' => 'Supprimer les éléments sélectionnés ?',
    'Refresh' => 'Rafraîchir',
    'Continue' => 'Continuer',
    'Sort' => 'Trier',
    'Sort order' => 'Sens du tri',
    'Export .CSV file' => 'Exporte un fichier CSV',
    'Add new' => 'Ajouter',
    'All' => 'Toutes',
    'Search description' => 'Chercher une description',
    'Search position' => 'Chercher une position',
    'Delete selected' => 'Supprimer la sélection',
    'Enable selection' => 'Activer la sélection',
    'Disable selection' => 'Désactiver la sélection',
    'Search email' => 'Chercher une adresse e-mail',
    'Search company' => 'Chercher une entreprise',
    'Search ISO code' => 'Chercher un code ISO',
    'Search code' => 'Chercher un code',
    'Search date format' => 'Chercher un format de date',
    'Search first name' => 'Chercher un prénom',
    'Search last name' => 'Chercher un nom de famille',
    'Search post code' => 'Chercher un code postal',
    'Search city' => 'Chercher une ville',
    'Search ID' => 'Chercher un ID',
    'Search name' => 'Chercher un nom',
    'Search key' => 'Chercher une clef',
    'Install' => 'Installer',
    'New product' => 'Nouveau produit',
    'Add tag' => 'Ajouter un mot-clé',
    'Export to SQL Manager' => 'Exporter vers le gestionnaire SQL',
    'Download' => 'Téléchargement',
    'Use' => 'Utiliser',
    'Change' => 'Modifier',
    'Load' => 'Charger',
    'Save and preview' => 'Enregistrer et prévisualiser',
    'Back to list' => 'Retour à la liste',
    'Learn more' => 'En savoir plus',
    'Generate emails' => 'Générer des e-mails',
    'Back to configuration' => 'Retour à la page de configuration',
    'Choose layouts' => 'Choisir la mise en page',
    'Enable' => 'Activer',
    'Edit: %value%' => 'Modification : %value%',
    'Copy' => 'Copier',
    'Export' => 'Exporter',
    'Modify' => 'Modifier',
    'Order by' => 'Trier par',
    'Try again' => 'Réessayer',
    'Let\'s go!' => 'C\'est parti !',
    'See less' => 'Voir moins',
    'See more' => 'Plus de détails',
    'Filter by categories' => 'Filtrer par catégories',
    'Unselect' => 'Désélectionner',
    'Activate selection' => 'Activer la sélection',
    'Deactivate selection' => 'Désactiver la sélection',
    'Duplicate selection' => 'Dupliquer la sélection',
    'Delete selection' => 'Supprimer la sélection',
    'Show SQL query' => 'Voir la requête SQL',
    'Reorder' => 'Re-commander',
    'Save & refresh' => 'Enregistrer et actualiser',
    'Delete now' => 'Supprimer',
    'Duplicate' => 'Dupliquer',
    'Expand' => 'Déployer',
    'Collapse' => 'Réduire',
    'Create' => 'Créer',
    'Download file' => 'Télécharger le fichier',
    'Delete this file' => 'Supprimer le fichier',
    'Read more' => 'En savoir plus',
    'Save image settings' => 'Enregistrer les paramètres d\'image',
    'Edit: %name%' => 'Modifier : %name%',
    'Add my IP' => 'Ajouter mon IP',
    'Choose file(s)' => 'Choisir un(des) fichier(s)',
    'Sort by' => 'Tri',
    'Check / Uncheck all' => 'Tout sélectionner / Tout désélectionner',
    'Confirm this action' => 'Confirmez cette action',
  ),
));

$catalogueDefault = new MessageCatalogue('default', array (
));
$catalogue->addFallbackCatalogue($catalogueDefault);

return $catalogue;
