<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @MailThemes/classic\assets\email.css */
class __TwigTemplate_54c50c4fc6638b75935ba06f911632d6ec3759d92398010911bd24dc7aed91e9 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@MailThemes/classic\\assets\\email.css"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "@MailThemes/classic\\assets\\email.css"));

        // line 1
        echo "body{
  background-color: #ffffff;
  width:650px;
  margin:auto;
  font-family: \"Open-sans\", sans-serif;
  color: #555454;
  font-size: 13px;
  line-height: 18px
}

table{width:100%;}

.table tbody > tr > td {
  padding:7px 0
}

td.space {width:20px;}
td.space_footer {padding:0 !important;}

table.table-mail{
  margin-top:10px;
  -moz-box-shadow: 0 0 5px #afafaf;
  -webkit-box-shadow: 0 0 5px #afafaf;
  -o-box-shadow: 0 0 5px #afafaf;
  box-shadow: 0 0 5px #afafaf;
  filter:progid:DXImageTransform.Microsoft.Shadow(color=#afafaf, Direction=134, Strength=5);
}

table.table-recap td{color:#777777;}
table.table-recap, table.table-recap .table {border-collapse: collapse;}
table.table-recap .table td{padding:0;}

.table .table-recap thead > tr > th{
  background-color: #fbfbfb;
  padding:10px;
  font-family: Arial;
  color:#333333;
  font-size: 13px
}

table.table-recap .conf_body td{color:#333333;}

table.table-recap .conf_body td.total{
  color:#555454;
  font-size:18px;
  font-weight:500;
  font-family: \"Open-sans\", sans-serif;
}

table.table-recap .conf_body td.total_amount{
  color:#333333;
  font-size: 21px;
  font-weight:500;
  font-family: \"Open-sans\", sans-serif;
}

a{color: #337ff1}

p{margin:3px 0 7px 0;}

span.title{
  font-weight: 500;
  font-size: 28px;
  text-transform: uppercase;
  line-height: 33px;
}
span.subtitle{
  font-weight: 500;
  font-size: 16px;
  text-transform: uppercase;
  line-height: 25px;
}

td.box{
  background-color: #f8f8f8;
}

td.box p{
  text-transform: uppercase;
  font-weight: 500;
  font-size: 18px;
  padding-bottom: 10px
}

td.box span{
  color:#777777;
}

td.box span span{
  color:#333333;
}

td.box ol{
  margin-bottom: 0
}
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@MailThemes/classic\\assets\\email.css";
    }

    public function getDebugInfo()
    {
        return array (  36 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("body{
  background-color: #ffffff;
  width:650px;
  margin:auto;
  font-family: \"Open-sans\", sans-serif;
  color: #555454;
  font-size: 13px;
  line-height: 18px
}

table{width:100%;}

.table tbody > tr > td {
  padding:7px 0
}

td.space {width:20px;}
td.space_footer {padding:0 !important;}

table.table-mail{
  margin-top:10px;
  -moz-box-shadow: 0 0 5px #afafaf;
  -webkit-box-shadow: 0 0 5px #afafaf;
  -o-box-shadow: 0 0 5px #afafaf;
  box-shadow: 0 0 5px #afafaf;
  filter:progid:DXImageTransform.Microsoft.Shadow(color=#afafaf, Direction=134, Strength=5);
}

table.table-recap td{color:#777777;}
table.table-recap, table.table-recap .table {border-collapse: collapse;}
table.table-recap .table td{padding:0;}

.table .table-recap thead > tr > th{
  background-color: #fbfbfb;
  padding:10px;
  font-family: Arial;
  color:#333333;
  font-size: 13px
}

table.table-recap .conf_body td{color:#333333;}

table.table-recap .conf_body td.total{
  color:#555454;
  font-size:18px;
  font-weight:500;
  font-family: \"Open-sans\", sans-serif;
}

table.table-recap .conf_body td.total_amount{
  color:#333333;
  font-size: 21px;
  font-weight:500;
  font-family: \"Open-sans\", sans-serif;
}

a{color: #337ff1}

p{margin:3px 0 7px 0;}

span.title{
  font-weight: 500;
  font-size: 28px;
  text-transform: uppercase;
  line-height: 33px;
}
span.subtitle{
  font-weight: 500;
  font-size: 16px;
  text-transform: uppercase;
  line-height: 25px;
}

td.box{
  background-color: #f8f8f8;
}

td.box p{
  text-transform: uppercase;
  font-weight: 500;
  font-size: 18px;
  padding-bottom: 10px
}

td.box span{
  color:#777777;
}

td.box span span{
  color:#333333;
}

td.box ol{
  margin-bottom: 0
}
", "@MailThemes/classic\\assets\\email.css", "C:\\wamp64\\www\\prestashop\\mails\\themes\\classic\\assets\\email.css");
    }
}
