<?php
/* Smarty version 3.1.33, created on 2020-07-01 10:15:15
  from 'C:\wamp64\www\prestashop\modules\flash_info\views\templates\hook\flashinfohome.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5efc3803afedf0_03173409',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1b59f288e8e4528ce9ef98b438d0cfbab3acb3b6' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\modules\\flash_info\\views\\templates\\hook\\flashinfohome.tpl',
      1 => 1593587712,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5efc3803afedf0_03173409 (Smarty_Internal_Template $_smarty_tpl) {
?> 
  <div class="card card_colonne" id="homeslider-container">
      <ul class="rslides">
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['flashinfo']->value, 'flash');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['flash']->value) {
?>
        <?php if ($_smarty_tpl->tpl_vars['flash']->value['id_category_monde'] == 1) {?> 
          <li class="cardinfo"> 
            <h3 class="titre">Actualités</h3> 
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['flashinfo_path'], ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['id_flash_info'], ENT_QUOTES, 'UTF-8');?>
.jpg" alt="Avatar" class="imginfo">
            <div class="col-md-10 descri">
              <p class="textedate"><span class="material-icons">event</span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['date'], ENT_QUOTES, 'UTF-8');?>
</p>
              <h5 class="textecatego">Actualité Nationale</h5>
              <p class="textecat">Categorie : <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['nom_category'], ENT_QUOTES, 'UTF-8');?>
</p>
              <p class="textetitre"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['title'], ENT_QUOTES, 'UTF-8');?>
</p>
            </div>
            <div class="col-md-2 voir">
              <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['link'], ENT_QUOTES, 'UTF-8');?>
" class="textevoir"><span class="material-icons"  style="font-size:35px !important;">visibility</span></a>
            </div>
          </li> 
        <?php } elseif ($_smarty_tpl->tpl_vars['flash']->value['id_category_monde'] == 2) {?>      
          <li class="cardinfo">  
            <h3 class="titre">Actualités</h3> 
            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['flashinfo_path'], ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['id_flash_info'], ENT_QUOTES, 'UTF-8');?>
.jpg" alt="Avatar" class="imginfo">
            <div class="col-md-10 descri">
              <p class="textedate"><span class="material-icons">event</span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['date'], ENT_QUOTES, 'UTF-8');?>
</p>
              <h5 class="textecatego">Actualité Nationale</h5>
              <p class="textecat">Categorie : <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['nom_category'], ENT_QUOTES, 'UTF-8');?>
</p>
              <p class="textetitre"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['title'], ENT_QUOTES, 'UTF-8');?>
</p>
            </div>
            <div class="col-md-2 voir">
              <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['flash']->value['link'], ENT_QUOTES, 'UTF-8');?>
" class="textevoir"><span class="material-icons view" >visibility</span></a>
            </div>
          </li> 
        <?php }?>  
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?> 
      </ul>
  </div>
</div> 
<?php }
}
