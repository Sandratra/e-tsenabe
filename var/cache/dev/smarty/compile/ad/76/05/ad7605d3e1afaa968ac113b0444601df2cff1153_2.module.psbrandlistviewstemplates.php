<?php
/* Smarty version 3.1.33, created on 2020-07-03 10:07:43
  from 'module:psbrandlistviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5efed93f9a2ef3_08873532',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ad7605d3e1afaa968ac113b0444601df2cff1153' => 
    array (
      0 => 'module:psbrandlistviewstemplates',
      1 => 1593755491,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:ps_brandlist/views/templates/_partials/".((string)$_smarty_tpl->tpl_vars[\'brand_display_type\']->value).".tpl' => 1,
  ),
),false)) {
function content_5efed93f9a2ef3_08873532 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin C:\wamp64\www\prestashop/themes/classic/modules/ps_brandlist/views/templates/hook/ps_brandlist.tpl -->
<div id="search_filters_brands">
  <section class="facet">
        <div>
      <?php if ($_smarty_tpl->tpl_vars['brands']->value) {?>
        <?php $_smarty_tpl->_subTemplateRender("module:ps_brandlist/views/templates/_partials/".((string)$_smarty_tpl->tpl_vars['brand_display_type']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('brands'=>$_smarty_tpl->tpl_vars['brands']->value), 0, true);
?>
      <?php } else { ?>
              <?php }?>
    </div>
  </section>
</div>
<!-- end C:\wamp64\www\prestashop/themes/classic/modules/ps_brandlist/views/templates/hook/ps_brandlist.tpl --><?php }
}
