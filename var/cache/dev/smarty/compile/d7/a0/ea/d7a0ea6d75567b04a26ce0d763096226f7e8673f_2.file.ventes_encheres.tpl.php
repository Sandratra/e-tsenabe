<?php
/* Smarty version 3.1.33, created on 2020-07-01 10:39:02
  from 'C:\wamp64\www\prestashop\modules\ventes_aux_encheres\views\templates\front\ventes_encheres.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5efc3d96c7b7e5_13194869',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd7a0ea6d75567b04a26ce0d763096226f7e8673f' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\modules\\ventes_aux_encheres\\views\\templates\\front\\ventes_encheres.tpl',
      1 => 1593589139,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5efc3d96c7b7e5_13194869 (Smarty_Internal_Template $_smarty_tpl) {
?>  <div class="card card_colonne" id="homeslider-container">
     <ul class="rslides">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['ventesauxencheres']->value, 'vente');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['vente']->value) {
?>
            <li class="cardvente">  
              <h3 class="titre">Ventes aux encheres</h3>
              <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vente']->value['url'], ENT_QUOTES, 'UTF-8');?>
"><img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['vente']->value['vente_path'], ENT_QUOTES, 'UTF-8');
echo htmlspecialchars($_smarty_tpl->tpl_vars['vente']->value['id_produit_enchere'], ENT_QUOTES, 'UTF-8');?>
.jpg" alt="Avatar" class="imgvente"></a>
            </li>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
      </ul>    
  </div><?php }
}
