<?php
/* Smarty version 3.1.33, created on 2020-07-01 09:45:15
  from 'C:\wamp64\www\prestashop\themes\classic\templates\_partials\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5efc30fb34f0d0_42354476',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a301dc9d64cea335adedebf392c04b7658b665fe' => 
    array (
      0 => 'C:\\wamp64\\www\\prestashop\\themes\\classic\\templates\\_partials\\header.tpl',
      1 => 1593494426,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5efc30fb34f0d0_42354476 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14037046945efc30fb33a7c3_17538618', 'header_top');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13718236735efc30fb3448b7_01170195', 'header_top');
?>

<?php }
/* {block 'header_top'} */
class Block_14037046945efc30fb33a7c3_17538618 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header_top' => 
  array (
    0 => 'Block_14037046945efc30fb33a7c3_17538618',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4" id="_desktop_logo">
          <h1 >
            <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
" >
              <img style="margin-top: 5px;" class="logo img-responsive animate__animated animate__tada animate__slow" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['logo'], ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['shop']->value['name'], ENT_QUOTES, 'UTF-8');?>
" >
            </a>
          </h1>
        </div>
        <div class="col-lg-5 col-md-4 col-sm-4 hidden-sm-down  position-static">
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displaySearch'),$_smarty_tpl ) );?>

          <div class="clearfix"></div>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-9 col-xs-8 connection">
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNav2'),$_smarty_tpl ) );?>

        </div>
      </div>
      <div class="col-xs-12 hidden-md-up position-static" style="margin-top: 5px;">
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displaySearch'),$_smarty_tpl ) );?>

        <div class="clearfix"></div>
      </div>
      
          </div>
  </div>
  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayNavFullWidth'),$_smarty_tpl ) );?>

<?php
}
}
/* {/block 'header_top'} */
/* {block 'header_top'} */
class Block_13718236735efc30fb3448b7_01170195 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'header_top' => 
  array (
    0 => 'Block_13718236735efc30fb3448b7_01170195',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <div class="header-top" style="padding-top:10px;">
    <div class="container">
      <div class="row">
        <div class=" col-md-12 col-sm-12 position-static menu-bottom">
          <div class="menu">
             <ul class="category-top-menu">
              <li data-depth="0" style="margin-right:-15px;">
                <span class="material-icons icon-home" style="margin-bottom:5px;">home</span>
              </li>
              <li data-depth="0" >
                <a class="cat category-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
" >
                Accueil
                </a>
              </li>
              <li data-depth="0">
                <a  class="cat category-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['about_us'], ENT_QUOTES, 'UTF-8');?>
" title="A propos de nous">
                Qui sommes nous?
                </a>
              </li>
              <li data-depth="0">
                <a class="cat category-link" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['contact'], ENT_QUOTES, 'UTF-8');?>
" title="Utiliser le formulaire pour nous contacter">
                Contactez-nous
                </a>
              </li>
            </ul>
          </div>
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayTop'),$_smarty_tpl ) );?>

          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
<?php
}
}
/* {/block 'header_top'} */
}
